FROM docker.io/library/alpine:latest
RUN apk upgrade --no-cache
RUN apk add --no-cache jq curl bash
